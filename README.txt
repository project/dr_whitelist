Comment whitelist (dr_whitelist) module

PREREQUISITES

A Drupal 6.x site where anonymous users have not been granted post 'comments
without approval' permission.

OVERVIEW

The Comment whitelist module manages list of name/email combinations for
anonymous users who are allowed to skip comment approval queue.

The module supports both manual and/or automatic (add to the whitelist when a
comment is published by an admin, delete from the whitelist when a comment is
deleted) management of the whitelist.

The list of current whitelist entries and an administrative interface can be
found under the 'Whitelist' tab at Administer > Content management > Comments.

INSTALLATION

No special procedures:
- upload and activate the module as usual;
- grant 'administer comment whitelist' permission as needed.

AUTHOR/MAINTAINER

Drave Robber
drave at right-here-tavern dot info

